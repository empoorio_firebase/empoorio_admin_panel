<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'El :attribute debe ser aceptado.',
    'accepted_if' => 'El :attribute debe ser aceptado cuando :other es :value.',
    'active_url' => 'El :attribute no es una URL válida.',
    'after' => 'El :attribute debe ser una fecha posterior a :value.',
    'after_or_equal' => 'El :attribute debe ser una fecha posterior o igual a :date.',
    'alpha' => 'El :attribute solo debe contener letras.',
    'alpha_dash' => 'El :attribute solo debe contener letras, números, guiones y guiones bajos.',
    'alpha_num' => 'El :attribute solo debe contener letras y números.',
    'array' => 'El :attribute debe ser una matriz.',
    'before' => 'El :attribute debe ser una fecha anterior a :date.',
    'before_or_equal' => 'El :attribute debe ser una fecha anterior o igual a :date.',
    'between' => [
         'numeric' => 'El :attribute debe estar entre :min y :max.',
         'file' => 'El :attribute debe estar entre :min y :max kilobytes.',
         'string' => 'El :attribute debe estar entre :min y :max caracteres.',
         'array' => 'El :attribute debe tener entre :min y :max elementos.',
     ],
     'boolean' => 'El campo :attribute debe ser verdadero o falso.',
     'confirmed' => 'El :attribute de confirmación no coincide.',
     'current_password' => 'La contraseña es incorrecta.',
     'date' => 'El :attribute no es una fecha válida.',
     'date_equals' => 'El :attribute debe ser una fecha igual a :date.',
     'date_format' => 'El :attribute no coincide con el formato :format.',
     'declined' => 'El :attribute debe ser rechazado.',
     'declined_if' => 'El :attribute debe rechazarse cuando :other es :value.',
     'different' => 'El :attribute y :other deben ser diferentes.',
     'digits' => 'El :attribute debe ser :digits digits.',
     'digits_between' => 'El :attribute debe estar entre :min y :max dígitos.',
     'dimensions' => 'El :attribute tiene dimensiones de imagen no válidas.',
     'distinct' => 'El campo :attribute tiene un valor duplicado.',
     'email' => 'El :attribute debe ser una dirección de correo electrónico válida.',
     'ends_with' => 'El :attribute debe terminar con uno de los siguientes: :values.',
     'enum' => 'El :attribute seleccionado no es válido.',
     'exists' => 'El :attribute seleccionado no es válido.',
     'file' => 'El :attribute debe ser un archivo.',
     'filled' => 'El campo :attribute debe tener un valor.',
     'gt' => [
         'numeric' => 'El :attribute debe ser mayor que :value.',
         'file' => 'El :attribute debe ser mayor que :value en kilobytes.',
         'string' => 'El :attribute debe ser mayor que :value carácteres.',
         'array' => 'El :attribute debe tener más de :value elementos.',
     ],
     'gte' => [
         'numeric' => 'El :attribute debe ser mayor o igual que :value.',
         'file' => 'El :attribute debe ser mayor o igual que :value en kilobytes.',
         'string' => 'El :attribute debe ser mayor o igual que :value caracteres.',
         'array' => 'El :attribute debe tener :value elementos o más.',
     ],
     'image' => 'El :attribute debe ser una imagen.',
     'in' => 'El :attribute seleccionado no es válido.',
     'in_array' => 'El campo :attribute no existe en :other.',
     'integer' => 'El :attribute debe ser un número entero.',
     'ip' => 'El :attribute debe ser una dirección IP válida.',
     'ipv4' => 'El :attribute debe ser una dirección IPv4 válida.',
     'ipv6' => 'El :attribute debe ser una dirección IPv6 válida.',
     'json' => 'El :attribute debe ser una cadena JSON válida.',
     'lt' => [
         'numeric' => 'El :attribute debe ser menor que :value.',
         'file' => 'El :attribute debe ser menor que :value kilobytes.',
         'string' => 'El :attribute debe ser menor que :value carácteres.',
         'array' => 'El :attribute debe tener menos de :value elementos.',
     ],
     'lte' => [
         'numeric' => 'El :attribute debe ser menor o igual que :value.',
         'file' => 'El :attribute debe ser menor o igual que :value en kilobytes.',
         'string' => 'El :attribute debe ser menor o igual que :value carácteres.',
         'array' => 'El :attribute no debe tener más de :value elementos.',
     ],
     'mac_address' => 'El :attribute debe ser una dirección MAC válida.',
     'max' => [
         'numeric' => 'El :attribute no debe ser mayor que :max.',
         'file' => 'El :attribute no debe ser mayor que :max kilobytes.',
         'string' => 'El :attribute no debe ser mayor que :max caracteres.',
         'array' => 'El :attribute no debe tener más de :máx. elementos.',
     ],
     'mimes' => 'El :attribute debe ser un archivo de tipo: :values.',
     'mimetypes' => 'El :attribute debe ser un archivo de tipo: :values.',
     'min' => [
         'numeric' => 'El :attribute debe ser al menos :min.',
         'file' => 'El :attribute debe tener al menos :min kilobytes.',
         'string' => 'El :attribute debe tener al menos :min caracteres.',
         'array' => 'El :attribute debe tener al menos :min elementos.',
     ],
     'multiple_of' => 'El :attribute debe ser un múltiplo de :value.',
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'password' => 'The password is incorrect.',
    'present' => 'The :attribute field must be present.',
    'prohibited' => 'The :attribute field is prohibited.',
    'prohibited_if' => 'The :attribute field is prohibited when :other is :value.',
    'prohibited_unless' => 'The :attribute field is prohibited unless :other is in :values.',
    'prohibits' => 'The :attribute field prohibits :other from being present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_array_keys' => 'The :attribute field must contain entries for: :values.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid timezone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute must be a valid URL.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],
];
